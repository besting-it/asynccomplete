/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bestingit.async;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Consumer;

/**
 * Instances of this class provide functionality for awaiting future tasks
 *
 * @author Andreas Besting, info@besting-it.de
 * @param <T> The result type
 */
public class Completer<T> {

    private Future<T> future;
    private final Queue<Consumer<Completer<T>>> completerConsumers = new ConcurrentLinkedQueue<>();
    private Consumer<T> typeConsumer;
    protected AsyncMethodInvocationException exception = new AsyncMethodInvocationException(null);
    protected T result;
    protected boolean success = false;

    protected Completer(Future<T> future) {
        this.future = future;
    }

    protected Completer(T result) {
        this.result = result;
    }

    protected Completer() {
    }

    protected T complete() {
        try {
            if (returnState()) // May throw exception
            {
                return result;
            }
            return future.get();
        } catch (AsyncMethodInvocationException | InterruptedException | ExecutionException ex) {
            if (ex.getCause() instanceof AsyncMethodInvocationException) {
                exception = (AsyncMethodInvocationException) ex.getCause();
            } else {
                exception = new AsyncMethodInvocationException(ex);
            }
        }
        if (returnState()) {
            return result;
        }
        throw new IllegalStateException("Completer reached illegal state.");
    }

    private boolean returnState() throws AsyncMethodInvocationException {
        if (isSuccess()) {
            return true;
        }
        if (isFaulted()) {
            throw exception;
        }
        return false;
    }

    protected void setFinishedSuccess(T result) {
        this.result = result;
        this.success = true;
        tryInvokeConsumers();
    }

    protected void setFinishedFailed(Exception exception) {
        this.exception = new AsyncMethodInvocationException(exception);
        this.success = false;
        tryInvokeConsumers();
    }

    /**
     * Accepts a consumer which will be called when the completer completes.
     * Consumer will be invoked in case of success or failure.
     *
     * @param consumer The consumer (completer type)
     * @return This completer
     */
    public Completer<T> result(Consumer<Completer<T>> consumer) {
        completerConsumers.add(consumer);
        tryInvokeConsumers();
        return this;
    }

    /**
     * Accepts a consumer which will be called when the completer completes.
     * Consumer will only be invoked in case of success. Exceptions are
     * swallowed silently.
     *
     * @param consumer The consumer (result type)
     * @return This completer
     */
    public Completer<T> feed(Consumer<T> consumer) {
        this.typeConsumer = consumer;
        tryInvokeConsumers();
        return this;
    }

    protected void tryInvokeConsumers() {
        if (isFaulted() || isSuccess()) {
            synchronized (completerConsumers) {
                completerConsumers.forEach(consumer -> consumer.accept(this));
                completerConsumers.clear();
            }
        }
        if (typeConsumer != null && isSuccess()) {
            typeConsumer.accept(result);
            typeConsumer = null;
        }
    }

    public AsyncMethodInvocationException getException() {
        return exception;
    }

    /**
     * Unwrap and rethrow if an exception has been detected in async processing.
     *
     * @throws Exception
     */
    public void unwrap() throws Exception {
        exception.unwrap();
    }

    public boolean isFaulted() {
        return exception.getCause() != null;
    }

    public boolean isSuccess() {
        return success;
    }

}
