/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bestingit.async;

import java.util.function.Consumer;

/**
 * Instances of this class provide functionality for awaiting future events
 *
 * @author Andreas Besting, info@besting-it.de
 * @param <T> The result type
 */
public class EventCompleter<T> extends Completer<T> {

    private final SyncEvent event = new SyncEvent();

    protected EventCompleter() {
    }

    @Override
    public T complete() {
        event.await(0);
        return super.complete();
    }

    /**
     * Wait until finished.
     *
     * @param timeout The timeout
     *
     * @return The result
     */
    public T complete(long timeout) {
        if (!event.await(timeout)) {
            setFinishedFailed(new AsyncTimeoutException("Task not completed within timeout."));
        }
        return super.complete();
    }

    /**
     * Successfully complete the event.
     *
     * @param result The result
     */
    @Override
    public void setFinishedSuccess(T result) {
        super.result = result;
        super.success = true;
        event.set();
        tryInvokeConsumers();
    }

    /**
     * Complete the event in case of a failure.
     *
     * @param exception The exception
     */
    @Override
    public void setFinishedFailed(Exception exception) {
        super.exception = new AsyncMethodInvocationException(exception);
        super.success = false;
        event.set();
        tryInvokeConsumers();
    }

    @Override
    public EventCompleter<T> result(Consumer<Completer<T>> consumer) {
        return (EventCompleter<T>) super.result(consumer);
    }

    @Override
    public EventCompleter<T> feed(Consumer<T> consumer) {
        return (EventCompleter<T>) super.feed(consumer);
    }

}
