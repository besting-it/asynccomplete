/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bestingit.async;

import java.util.Collections;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

/**
 * Async-Complete (aka Async-Await) is a library for writing asynchronous code
 * in a more sequential style. It provides executing and interrupting async,
 * background tasks and executing event-based code using a bounded or unbounded
 * thread pool, unchecked exception handling and easy scheduling and cancelling
 * of (recurring) tasks.
 *
 * @author Andreas Besting, info@besting-it.de
 * @version 2.0.5
 */
public class Task {

    private static final Set<Long> CANCELLED_IDS = Collections.newSetFromMap(new ConcurrentHashMap<Long, Boolean>());
    private static final Timer TIMER = new Timer();
    private static final AtomicLong SCHEDULED_IDS = new AtomicLong(-1);
    private static final AsyncExecutorService SERVICE = new AsyncExecutorService();

    public static void setThreadPoolSize(int size) {
        SERVICE.setThreadPoolSize(size);
    }

    /**
     * Executes a task asynchronously without interrupt control.
     *
     * The number of threads will not exceed the specified fixed thread pool
     * size.
     *
     * Note that tasks may be queued. You can monitor the current queue size for
     * optimizing purposes.
     *
     * @param <T> The task return type
     * @param task The task
     *
     * @return The completer
     */
    public static <T> Completer<T> async(Callable<T> task) {
        return async((i) -> {
            try {
                return task.call();
            } catch (Exception ex) {
                throw new AsyncMethodInvocationException(ex);
            }
        }, new CancellationInterrupt());
    }

    /**
     * Executes a task asynchronously.
     *
     * The number of threads will not exceed the specified fixed thread pool
     * size.
     *
     * Note that tasks may be queued. You can monitor the current queue size for
     * optimizing purposes.
     *
     * @param <T> The task return type
     * @param task The task
     * @param interrupt The interrupt
     *
     * @return The completer
     */
    public static <T> Completer<T> async(AsyncCallable<T> task, AbstractTaskInterrupt interrupt) {
        return SERVICE.executeBounded(task, interrupt);
    }

    /**
     * Executes a task asynchronously without interrupt control.
     *
     * The number of threads will not exceed the specified fixed thread pool
     * size.
     *
     * Note that tasks may be queued. You can monitor the current queue size for
     * optimizing purposes.
     *
     * @param task The task
     *
     * @return The completer
     */
    public static Completer async(Runnable task) {
        return async((i) -> {
            task.run();
            return null;
        }, new CancellationInterrupt());
    }

    /**
     * Executes a task asynchronously.
     *
     * The number of threads will not exceed the specified fixed thread pool
     * size.
     *
     * Note that tasks may be queued. You can monitor the current queue size for
     * optimizing purposes.
     *
     * @param task The task
     * @param interrupt The interrupt
     *
     * @return The completer
     */
    public static Completer async(AsyncRunnable task, AbstractTaskInterrupt interrupt) {
        return async((i) -> {
            task.run(i);
            return null;
        }, interrupt);
    }

    /**
     * Executes asynchronously using unbounded cached threadpool without
     * interrupt control
     *
     * @param <T> The task return type
     * @param task The task
     *
     * @return The completer
     */
    public static <T> Completer<T> blocking(Callable<T> task) {
        return blocking((i) -> {
            try {
                return task.call();
            } catch (Exception ex) {
                throw new AsyncMethodInvocationException(ex);
            }
        }, new CancellationInterrupt());
    }

    /**
     * Executes asynchronously using unbounded cached threadpool
     *
     * @param <T> The task return type
     * @param task The task
     * @param interrupt The interrupt
     *
     * @return The completer
     */
    public static <T> Completer<T> blocking(AsyncCallable<T> task, AbstractTaskInterrupt interrupt) {
        return SERVICE.executeUnbounded(task, interrupt);
    }

    /**
     * Executes asynchronously using unbounded cached threadpool without
     * interrupt control
     *
     * @param task The task
     *
     * @return The completer
     */
    public static Completer blocking(Runnable task) {
        return blocking((i) -> {
            task.run();
            return null;
        }, new CancellationInterrupt());
    }

    /**
     * Executes asynchronously using unbounded cached threadpool
     *
     * @param task The task
     * @param interrupt The interrupt
     *
     * @return The completer
     */
    public static Completer blocking(AsyncRunnable task, AbstractTaskInterrupt interrupt) {
        return blocking((i) -> {
            task.run(i);
            return null;
        }, interrupt);
    }

    /**
     * Produces an event-based completer
     *
     * @param <T> The task return type
     * @param resultType
     *
     * @return The completer
     */
    public static <T> EventCompleter<T> event(Class<T> resultType) {
        return new EventCompleter<>();
    }

    /**
     * Waits until the provided completer has been completed and returns the
     * result. Eventually throws an unchecked AsyncMethodInvocationException.
     *
     * @param <T> The task return type
     * @param completer The completer
     *
     * @return The result
     */
    public static <T> T complete(Completer<T> completer) {
        return completer.complete();
    }

    /**
     * Waits until the provided completer has been completed and returns the
     * result. Eventually throws an unchecked AsyncMethodInvocationException.
     *
     * @param timeout The timeout in ms (0 to disable)
     * @param <T> The task return type
     * @param completer The completer
     *
     * @return The result
     * @throws AsyncTimeoutException in case of a timeout
     */
    public static <T> T complete(long timeout, Completer<T> completer) {
        return (T) any(timeout, completer).complete();
    }

    /**
     * Waits until all completers have been completed.
     *
     * @param completer The completers
     */
    public static void all(Completer... completer) {
        all(0, completer);
    }

    /**
     * Waits until all completers have been completed.
     *
     * @param timeout The timeout in ms (0 to disable)
     * @param completer The completers
     *
     * @throws AsyncTimeoutException in case of a timeout
     */
    public static void all(long timeout, Completer... completer) {
        SyncEvent event = new SyncEvent();
        AtomicInteger counter = new AtomicInteger();
        for (Completer next : completer) {
            next.result(c -> {
                counter.incrementAndGet();
                if (counter.get() == completer.length) {
                    event.set();
                }
            });
        }
        if (!event.await(timeout)) {
            throw new AsyncTimeoutException("Waiting for all timed out.");
        }
    }

    /**
     * Waits until first completer completes.
     *
     * @param completer The completers
     * @return First completer
     */
    public static Completer any(Completer... completer) {
        return any(0, completer);
    }

    /**
     * Waits until first completer completes.
     *
     * @param timeout The timeout in ms (0 to disable)
     * @param completer The completers
     *
     * @return First completer
     * @throws AsyncTimeoutException in case of a timeout
     */
    public static Completer any(long timeout, Completer... completer) {
        SyncEvent event = new SyncEvent();
        Completer[] first = new Completer[1];
        for (Completer next : completer) {
            next.result(c -> {
                if (first[0] == null) {
                    first[0] = (Completer) c;
                    event.set();
                }
            });
        }
        if (!event.await(timeout)) {
            throw new AsyncTimeoutException("Waiting for any timed out.");
        }
        return first[0];
    }

    /**
     * Schedules an async task. Note that any exception will be swallowed
     * silently (executing will continue in case of recurring). You have to
     * handle exceptions yourself in the runnable.
     *
     * @param runnable The task
     * @param delayMillis Interval delay in ms
     * @param recurring Whether to repeat this task until cancelled
     *
     * @return An ID for cancelling
     */
    public static long scheduleAsync(Runnable runnable, long delayMillis, boolean recurring) {
        long id = SCHEDULED_IDS.incrementAndGet();
        schedule(runnable, delayMillis, recurring, false, id);
        return id;
    }

    /**
     * Schedules a blocking task. Note that any exception will be swallowed
     * silently (executing will continue in case of recurring). You have to
     * handle exceptions yourself in the runnable.
     *
     * @param runnable The task
     * @param delayMillis Interval delay in ms
     * @param recurring Whether to repeat this task until cancelled
     *
     * @return An ID for cancelling
     */
    public static long scheduleBlocking(Runnable runnable, long delayMillis, boolean recurring) {
        long id = SCHEDULED_IDS.incrementAndGet();
        schedule(runnable, delayMillis, recurring, true, id);
        return id;
    }

    private static synchronized void schedule(Runnable runnable, long delayMillis, boolean recurring, boolean blocking, long id) {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                // Check if we should cancel
                if (CANCELLED_IDS.contains(id)) {
                    CANCELLED_IDS.remove(id);
                    return;
                }
                // Create a consumer task to eventually reschedule in case of recurring
                final Consumer<Completer<Object>> task = c -> {
                    if (recurring) {
                        schedule(runnable, delayMillis, recurring, blocking, id);
                    }
                };
                // Execute the runnable and register our reschedule task on finish
                if (blocking) {
                    blocking(runnable).result(task);
                } else {
                    async(runnable).result(task);
                }
            }
        };
        // Schedule one-shot
        TIMER.schedule(timerTask, delayMillis);
    }

    /**
     * Cancels a scheduled task
     *
     * @param id The ID of the task
     */
    public static void cancel(long id) {
        CANCELLED_IDS.add(id);
    }

    /**
     * Sleeps.
     *
     * @param millis Time in ms
     */
    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            throw new AsyncMethodInvocationException(ex);
        }
    }

}
