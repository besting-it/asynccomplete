/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bestingit.async;

/**
 * Instance of this class provides unchecked exception handling.
 *
 * @author Andreas Besting, info@besting-it.de
 */
public class AsyncMethodInvocationException extends RuntimeException {

    private final Exception inner;

    public AsyncMethodInvocationException(Exception inner) {
        this.inner = inner;
    }

    @Override
    public Exception getCause() {
        return inner;
    }

    public Exception getUnwrappedCause() {
        try {
            unwrap();
        } catch (Exception e) {
            return e;
        }
        return null;
    }

    public void unwrap() throws Exception {
        if (inner == null) {
            return;
        }
        Exception ex = inner;
        while (ex.getCause() instanceof AsyncMethodInvocationException) {
            ex = (AsyncMethodInvocationException) ex.getCause();
        }
        if (ex instanceof AsyncMethodInvocationException) {
            throw ((AsyncMethodInvocationException) ex).inner;
        }
        throw inner;
    }

}
