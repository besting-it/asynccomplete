/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bestingit.async;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class implements an event for waiting and signalling to resume.
 *
 * @author Andreas Besting, info@besting-it.de
 */
public class SyncEvent {

    private final Object sync = new Object();
    private final AtomicBoolean sig = new AtomicBoolean(false);

    public SyncEvent() {
    }

    public boolean await(long timeout) {
        if (sig.get()) {
            sig.set(false);
            return true;
        }
        synchronized (sync) {
            try {
                sync.wait(timeout);
            } catch (InterruptedException ex) {
            }
        }
        boolean res = sig.get();
        sig.set(false);
        return res;
    }

    public void set() {
        set(true);
    }

    public void set(boolean state) {
        sig.set(state);
        synchronized (sync) {
            sync.notifyAll();
        }
    }

}
