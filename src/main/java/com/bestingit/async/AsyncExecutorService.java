/**
 * Copyright 2019 Andreas Besting, info@besting-it.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.bestingit.async;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncExecutorService implements AutoCloseable {

    class QueuedTask {

        AsyncCallable callable;
        EventCompleter completer;
        AbstractTaskInterrupt interrupt;
    }

    private static final int DEFAULT_FIXED_POOL_SIZE = 8;
    private final BlockingQueue<QueuedTask> tasks = new LinkedBlockingQueue<>();
    private final ThreadPoolExecutor service = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
            60L, TimeUnit.SECONDS,
            new SynchronousQueue<>());
    private final ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
    private final Set<Long> threadIds = Collections.newSetFromMap(new ConcurrentHashMap<>());

    private int threadPoolSize = DEFAULT_FIXED_POOL_SIZE;
    private AtomicInteger threadCount = new AtomicInteger();
    private boolean closed;

    public AsyncExecutorService() {
        initQueueProcessing();
    }

    @Override
    public void close() throws Exception {
        closed = true;
    }

    private void initQueueProcessing() {
        singleExecutor.execute(() -> {
            threadCount.incrementAndGet();
            final long id = Thread.currentThread().getId();
            threadIds.add(id);
            while (!closed) {
                QueuedTask task = null;
                try {
                    task = tasks.take();
                    final Object call = task.callable.call(task.interrupt);
                    task.completer.setFinishedSuccess(call);
                } catch (Exception e) {
                    task.completer.setFinishedFailed(e);
                }
            }
            threadIds.remove(Thread.currentThread().getId());
        });
    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public synchronized <T> Completer<T> executeBounded(AsyncCallable<T> task, AbstractTaskInterrupt interrupt) {
        if (threadCount.get() > threadPoolSize) {
            long curId = Thread.currentThread().getId();
            if (threadIds.contains(curId)) {
                // Execute synchronous in an already async context
                Completer<T> completer = new Completer<>();
                try {
                    T result = task.call(interrupt);
                    completer.setFinishedSuccess(result);
                } catch (Exception e) {
                    completer.setFinishedFailed(e);
                }
                return completer;
            }
            // Task may be safely queued
            QueuedTask qt = new QueuedTask();
            qt.callable = task;
            qt.completer = new EventCompleter<>();
            qt.interrupt = interrupt;
            tasks.add(qt);
            return qt.completer;
        }
        return executeUnbounded(task, interrupt);
    }

    public int getCurrentThreadCount() {
        return threadCount.get();
    }

    public synchronized <T> Completer<T> executeUnbounded(AsyncCallable<T> task, AbstractTaskInterrupt interrupt) {
        threadCount.incrementAndGet();
        Completer<T>[] completerRef = new Completer[1];
        SyncEvent event = new SyncEvent();
        completerRef[0] = new Completer<>(service.submit(() -> {
            long newId = Thread.currentThread().getId();
            threadIds.add(newId);
            event.await(0);
            Completer completer = completerRef[0];
            try {
                T result = task.call(interrupt);
                completer.setFinishedSuccess(result);
                return result;
            } catch (Exception e) {
                completer.setFinishedFailed(e);
                throw new AsyncMethodInvocationException(e);
            } finally {
                threadIds.remove(newId);
                threadCount.decrementAndGet();
            }
        }));
        event.set();
        return completerRef[0];
    }

}
