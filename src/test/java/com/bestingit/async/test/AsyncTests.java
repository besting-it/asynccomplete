package com.bestingit.async.test;

import com.bestingit.async.AsyncCancellationException;
import static com.bestingit.async.Task.*;
import static org.junit.Assert.*;
import com.bestingit.async.AsyncMethodInvocationException;
import com.bestingit.async.AsyncTimeoutException;
import com.bestingit.async.Completer;
import com.bestingit.async.EventCompleter;
import com.bestingit.async.SyncEvent;
import com.bestingit.async.TimeoutInterrupt;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AsyncTests {

    private SyncEvent testEvent;
    private String testResult;
    private int testTime;

    public AsyncTests() {
    }

    @BeforeClass
    public static void setUpClass() {
        setThreadPoolSize(8);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        testEvent = new SyncEvent();
        testResult = "";
        testTime = 1000;
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTimeout() {
        System.out.println("testTimeout");
        List<Completer> list = new ArrayList<>();
        boolean timeoutException = false;
        for (int i = 0; i < 10; i++) {
            list.add(blocking(() -> {
                sleep(2000);
            }));
        }
        try {
            all(1000, list.toArray(new Completer[1]));
        } catch (AsyncTimeoutException ex) {
            timeoutException = true;
        }
        assertTrue(timeoutException);
    }

    @Test
    public void testInterrupt() throws IOException {
        System.out.println("testInterrupt");
        TimeoutInterrupt ti = new TimeoutInterrupt(1000);
        boolean interruptException = false;
        try {
            complete(async((interrupt) -> {
                for (int i = 0; i < 20; i++) {
                    sleep(100);
                    interrupt.throwIfInterrupted();
                }
            }, ti));
        } catch (AsyncMethodInvocationException ex) {
            Exception e = ex.getUnwrappedCause();
            interruptException = (e instanceof AsyncCancellationException);
        }
        assertTrue(interruptException);
    }

    @Test
    public void testCompleterFeed() throws IOException {
        System.out.println("testCompleterFeed");
        // Without exception handling
        addToString(1, 2, 3, 4).feed(this::setResult);
        testEvent.await(testTime);
        assertEquals("10", testResult);
    }

    @Test
    public void testCompleterResult() throws IOException {
        System.out.println("testCompleterResult");
        // Without exception handling
        addToString(1, 2, 3, 4).result(res -> {
            setResult(complete(res));
        });
        testEvent.await(Integer.MAX_VALUE);
        assertEquals("10", testResult);
    }

    @Test
    public void testCompleterException() throws IOException {
        System.out.println("testCompleterException");
        addToString(1, 2, 0, 0).result(res -> {
            try {
                res.unwrap();
            } catch (RuntimeException ex) {
                testResult = ex.getMessage();
                testEvent.set(true);
            } catch (Exception ex) {
            }
        });//.feed(s -> outputCompleter(s)); // In case of success
        testEvent.await(testTime);
        assertEquals("Exception", testResult);
    }

    @Test
    public void testEventSource() throws IOException {
        System.out.println("testEventSource");
        EventCompleter<String> ec = event(String.class).feed(this::setResult);
        // Schedule finisher
        scheduleBlocking(() -> {
            ec.setFinishedSuccess("Test");
        }, testTime / 2, false);
        // Test completer
        String res = complete(ec);
        assertEquals("Test", res);
        // Test feed
        testEvent.await(testTime);
        assertEquals("Test", testResult);
    }

    @Test
    public void testEventSourceException() throws IOException {
        System.out.println("testEventSourceException");
        EventCompleter<String> ec = event(String.class);//.feed(this::setResult);  // In case of success
        // Schedule finisher
        scheduleBlocking(() -> {
            ec.setFinishedFailed(new RuntimeException("Exception"));
        }, testTime / 2, false);
        // Test completer
        try {
            complete(ec);
        } catch (AsyncMethodInvocationException ex) {
            Exception e = ex.getUnwrappedCause();
            if (e instanceof RuntimeException) {
                testResult = e.getMessage();
                testEvent.set(true);
            }
        }
        testEvent.await(testTime);
        assertEquals("Exception", testResult);
    }

    @Test
    public void testAny() {
        System.out.println("testAny");
        EventCompleter<String> ec1 = event(String.class);
        EventCompleter<String> ec2 = event(String.class);
        // Schedule finishers
        scheduleAsync(() -> {
            ec1.setFinishedSuccess("TestLong");
        }, testTime / 2, false);
        scheduleAsync(() -> {
            ec1.setFinishedSuccess("TestShort");
        }, testTime / 4, false);
        Completer first = any(ec1, ec2);
        String str = (String) complete(first);
        assertEquals("TestShort", str);
    }

    private Completer<String> addToString(int i, int j, int k, int l) {
        return async(() -> {
            int res = i + complete(add(j, k, l));
            return Integer.toString(res);
        });
    }

    private Completer<Integer> add(int m, int n, int o) {
        return async(() -> {
            int res = m + complete(add(n, o));
            return res;
        });
    }

    private Completer<Integer> add(int p, int q) {
        return async(() -> {
            int res = p + complete(ret(q));
            if (res == 0) {
                throw new RuntimeException("Exception");
            }
            return res;
        });
    }

    private Completer<Integer> ret(int x) {
        return async(() -> {
            return complete(async(() -> {
                return x;
            }));
        });
    }

    private void setResult(String str) {
        testResult = str;
        testEvent.set(true);
    }
}
