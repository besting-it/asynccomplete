# asynccomplete

Async-Complete (aka Async-Await) is a library for writing asynchronous code in a more sequential style.
It provides executing and interrupting async, background and event-based tasks using a bounded or unbounded thread pool, unchecked exception handling and easy scheduling and cancelling of (recurring) tasks.
Async-Complete differs in that it takes the calling context into account by keeping track of thread IDs.

Licensed under the Apache License, Version 2.0.

# Quick start
Start by importing Task's static functions:

```java
import static com.bestingit.async.Task.*;
```

## Writing async methods
Suppose a convential method looks like this:

```java
private int add(int a, int b) {
	return a + b;
}
```

Then the async-style method would look like this:
```java
private Completer<Integer> add(int a, int b) {
    return async(() -> {
        return a + b;
    });         
}
```

## Completing and processing async methods
Waiting for a result to complete:

```java
int res = complete(add(1, 1));  // res == 2
```

Processing asynchronously or feeding the result to another function:
```java
add(1, 1).feed(this::print);  // This line won't block your thread

private void print(int i) {
	System.out.println(i);  // prints "2"
}
```

## How do I get set up ? ###

* Create a Java maven project

* Add the following dependency to pom.xml
```
<project>
  ...
  <dependencies>
	<dependency>
	  <groupId>de.besting-it</groupId>
	  <artifactId>asynccomplete</artifactId>
	  <version>2.0.5</version>
	</dependency>
      ...
  </dependencies>
</project>
```

# Documentation

## How Async-Complete works

The _Task_ class offers several static functions for _async_-, and _blocking_- and _event_-based execution. Async and blocking functions accept either a _Runnable_ or a _Callable<T>_ task.
In case of events, only the result-type must be provided. Each function returns a _Completer<T>_. Both executer- and completer-functions are explained below.

### Executer-Functions

#### Completer<T> async(Callable<T> task / Runnable task)

Calling this function will execute the runnable or callable task asynchronous. Usually that means submitting to a fixed-size thread pool, possibly queueing in case the limit is exceeded. But the executing strategy actually depends on the calling context.
In case of calling from an already async context, submitting to the threadpool will only happen if free workers are available. If not, the default case is to execute synchronous in an _already_ async worker (continuation), thus still not blocking the main thread.

For keeping an upper thread count bound, tasks may be queued. You can set the maximum number of threads used for async using the following code:
```java
setThreadPoolSize(2);  // Minimum thread count for async is 1, default is 8
```
Note that code executed in the async case should not block for a long time since the number of threads are limited. What that means in practice depends on the concrete application, more precise on how much response time you're willing to trade to creating unlimited threads, which comes with an overhead on its own (for the latter, see the blocking case explained below).

#### Completer<T> blocking(Callable<T> task / Runnable task)

Calling this function will execute the runnable or callable task containing blocking code. That means submitting to a cached thread pool. There is no upper bound for the number of workers, but threads may be reused. Tasks won't be queued.
If you're familiar with C# this is similar to calling Task.Run(() => { ... }). So in contrast to _async_, _blocking_ always means multithreading.
In case you have some heavy parallel computation or other long-running / waiting tasks, prefer blocking over async.

#### EventCompleter<T> event(Class<T> resultType)

Calling this function will simply create an event-source represented by a special kind of completer that can be used in an async context like the default completer.
The event-completer can be set _finished_ in case of success (result) or failure (exception). Nothing will be executed on any thread pool since the result itself will be provided by your code.
Using the event-source can provide methods for wrapping other event-based or asynchronous code into working with this library. If you're familiar with C# this is similar to a TaskCompletionSource.

### Completer-Functions

A Completer holds a promise to a future result. There are three different methods for handling a completer.

* __complete()__: Waits until finished and returns the result. If already finihsed, the result will be returned instantaneous. Eventually throws an unchecked AsyncMethodInvocationException.
* __feed(Consumer<T> consumer)__: Accepts a consumer which will be called when the completer completes. The consumer will only be invoked in case of success, exceptions are swallowed silently.
* __result(Consumer<Completer<T>> consumer)__: Accepts a consumer which will be called when the completer completes. Consumer will be invoked in case of success or failure.

#### Using complete inside async or blocking

The _complete_ call can be used anywhere inside _async_ or _blocking_ contexts:
```java
private Completer<Integer> calculate(int a, int b, int c) {
    return async(() -> {
        int x = complete(div(b, c));
        return a + x;
    });         
}

private Completer<Integer> div(int a, int b) {
    return async(() -> {
        return a / b;
    });         
}
```

The function _div(a, b)_ called by _calculate(a, b, c)_ might be executed in a seperate thread or in the same thread as the function _calculate(a, b, c)_. Since _div(a, b)_ cannot be queued because of the waiting of the completer (which would result in a deadlock), the kind of execution depends on the number of currently free workers.

#### Exception handling

The ususal pattern for executing and producing a completer involves either calling _result_ (in case of exception checking) and _feed_ (in case of success). You can also combine both.

Take the following extended example:
```java
// Suppose div may throw a exception
private Completer<Integer> div(int a, int b) {
    return async(() -> {
        if (b == 0)
            throw new RuntimeException("0 not allowed!");
        return a / b;
    });         
}
```
The following example shows direct catching of exceptions when calling _complete_:
```java
Completer<Integer> c = div(a, b);
try {
	int i = complete(c);  // Block and wait
} catch (AsyncMethodInvocationException ex) {
	Exception e = ex.getUnwrappedCause();
	if (e instanceof RuntimeException) {
	   System.err.println(e.getMessage());
	}
}  
```

The following example shows catching of exceptions in the functional-style _result_ and _feeding_ in case of success. Note that the syntax is very similar, the try-catch block just moves into the lambda function.
```java
div(a, b).result(res -> {
     try {     
         res.unwrap();  // Unwraps and throws exception
     } catch (RuntimeException ex) {
        System.err.println(ex.getMessage());
     } catch (Exception ex) {
     }                
}).feed(this::print);  // In case of success
```

The same thing can be achieved by ommitting _feed_ and calling complete in the _result_ part.
```java
div(a, b).result(res -> {
     try {     
         res.unwrap();  // Unwraps and throws exception
         int i = complete(res);  // In case of success, returns instantaneous
         print(i);
     } catch (RuntimeException ex) {
        System.err.println(ex.getMessage());
     } catch (Exception ex) {
     }                
});
```

The following example shows ignoring all exceptions while processing functional-style, like shown in the first example:
```java
div(1, 1).feed(System.out::println);
```

#### Awaiting multiple completers, doing work in between

The _complete_ method can also be called later to do things in between:

```java
private Completer<Integer> calculate(int a, int b, int c) {
    return async(() -> {
        Completer<Integer> c = div(b, c);
        // Here you can do other things
        System.out.println("Maybe calculating now, depends on the context");
        return a + complete(c);
    });         
}
```

Example pattern for completing _all_ tasks:
```java
Completer<String> one = createSomeStringAsync();  // async function
Completer<String> two = createSomeOtherStringAsync();  // async function
Completer<String> three = async(() -> ... );

// Here, you can do other things

all(one, two, three); // Wait for all tasks to complete

String oneResult = complete(one); // Will return instantaneous if already complete
```

Example pattern for doing things in parallel:
```java
Completer<Integer> one = heavyCalculateSomethingBlocking();  // blocking function
Completer<Integer> two = heavyCalculateSomethingElseBlocking();  // blocking function
Completer<Integer> three = blocking(() -> ... );

// Here, you can do other things

all(one, two, three); // Wait for all tasks to complete, tasks running parallel
```

Waiting for _any_ task to complete:
```java
Completer first = any(one, two, three); // Wait for the first task to complete (cast needed)
```

## Scheduling

You can schedule tasks on both async and blocking contexts using _scheduleAsync_ and _scheduleBlocking_.

Here's a one-shot task printing out something after one second:
```java
scheduleAsync(() -> {
	System.out.println("Test");
}, 1000, false);
```

By setting the last parameter to true, the task will be executed recurrently (here, in a blocking context, i.e. in a thread):
```java
long id = scheduleBlocking(() -> {
	System.out.println("Test, again, and again...");
}, 1000, true);
```

Cancelling the scheduled process using the id:
```java
cancel(id);
```

## Eventing

The _event_ function creates a special completer that can be used like a default completer. You can use the _setFinished_ and _setFailed_ functions for setting the result or an exception.

```java
// Create the event source
EventCompleter<String> source = event(String.class);
// Set the result after one second
scheduleBlocking(() -> {
	source.setFinishedSucess("Test");
}, 1000, false);
String result = complete(source);
```

Here's an example for setting an exception:
```java
// Create the event source
EventCompleter<String> source = event(String.class);
// Set the result after one second
scheduleBlocking(() -> {
    source.setFinishedFailed(new RuntimeException("Failed"));
}, 1000, false);
try {
	String result = complete(source);
} catch (AsyncMethodInvocationException ex) {
	Exception e = ex.getUnwrappedCause();
	if (e instanceof RuntimeException) {
	   String message = e.getMessage();  // "Failed"
	}
} 
```

Another one using _any_:
```java
EventCompleter<String> source1 = event(String.class);
EventCompleter<String> source2 = event(String.class);
// Schedule finishers
scheduleBlocking(() -> {
	source1.setFinishedSucess("TestLong");
}, 2000, false);
scheduleBlocking(() -> {
	source1.setFinishedSucess("TestShort");
}, 1000, false);
Completer first = any(source1, source2);
String str = (String)complete(first);
System.out.println(str); // "TestShort"
```

## Timeouts

The functions _complete_, _all_ and _any_ are overloaded and can be called with a timeout parameter. An AsyncTimeoutException will be thrown in case a timeout expires before the execution has finished.
Note that throwing a timeout exceptions does not mean that the underlying task will be interrupted (see next section).

## Interruption

The methods _async_ and _blocking_ are overloaded for dispatching a special kind of runnable and callable together with a concrete instance of _AbstractTaskInterrupt_. There are two kinds of interruption:

* __TimeoutInterrupt__: A token which sets itself cancelled when a timeout expires.
* __CancellationInterrupt__: A token which may be set cancelled.

In both cases the user code should call the _throwIfInterrupted()_ method where appropriate to add cancellation support.

Example for TimeoutInterrupt (cancel loop after 1 second):
```java
TimeoutInterrupt ti = new TimeoutInterrupt(1000);
boolean interrupted = false;
try {
    complete(async((interrupt) -> {
        for (int i = 0; i < 20; i++) {
            sleep(100);
            interrupt.throwIfInterrupted();
        }
    }, ti));
} catch (AsyncMethodInvocationException ex) {
    Exception e = ex.getUnwrappedCause();
    interruptException = (e instanceof AsyncCancellationException);
}
System.out.println(interrupted); // true
```

The usage of __CancellationInterrupt__ is similar, just call __setInterrupted(true)__ manually.